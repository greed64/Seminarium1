﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminarium1
{
    class Program
    {
        static void Main(string[] args)
        {
            Product[] productsArray = {
                new Product ("produkt1", Product.CategoryEnum.koszykówka,99M ),
                new Product ("produkt2", Product.CategoryEnum.siatkówka,70M ),
                new Product ("produkt3", Product.CategoryEnum.golf,199.99M ),
                new Product ("produkt4", Product.CategoryEnum.koszykówka,144.99M )
            };

            List<Product> productsList = productsArray.ToList();

            IQueryable<Product> productQueryable = productsArray.AsQueryable();

            IEnumerable<Product> productsEnumerable = productsArray.AsEnumerable();

            SortExample.sortComparator(productsArray);

            Console.ReadKey();
        }

      
    }
}
