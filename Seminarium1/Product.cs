﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminarium1
{

    class Product
    {
        
        public enum CategoryEnum { koszykówka, siatkówka, golf };

        public string Name {get; set; }

        public CategoryEnum Category { get; set; }

        public decimal Prize { get; set; }

        public Product(String name, CategoryEnum category, decimal prize)
        {
            Name = name;
            Category = category;
            Prize = prize;
        }

        public override string ToString()
        {
            return Prize + " " + Name + " "  + Category.ToString() ;
        }
    }
}
