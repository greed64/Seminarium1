﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminarium1
{
    class SortExample
    {
        public static void sortComparator(IEnumerable<Product> products) {

            var classicProductList = clasicSortMethod(products);

            var LINQ_SQLSyntaxProductList = from product in products
                                            orderby product.Prize 
                                            select product;

            var LINQ_FuncionSyntaxProductList = products.OrderBy(p => p.Prize);

            mySimplePrintMethod("---Klasyczne sortowanie---", classicProductList);

            mySimplePrintMethod("---LINQ składnia SQL sortowanie---", LINQ_SQLSyntaxProductList);

            mySimplePrintMethod("---LINQ składnia funkcyjna sortowanie---", LINQ_FuncionSyntaxProductList);
           
        }

        public static IEnumerable<Product> clasicSortMethod(IEnumerable<Product> products)
        {
            Array.Sort(products.ToArray(), (item1, item2) =>
            {
                return Comparer<Decimal>.Default.Compare(item1.Prize, item2.Prize);
            });

            return products;
        }

        public static void mySimplePrintMethod(String message, IEnumerable<Product> products) {
            Console.WriteLine("\n"+message);
            foreach (Product product in products)
            {
                Console.WriteLine(product);
            }
        }
    }

   
}
